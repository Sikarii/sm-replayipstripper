// ================== DOUBLE INCLUDE ========================= //

#if defined _ReplayIPStripper_included_
#endinput
#endif
#define _ReplayIPStripper_included_

// =========================================================== //

#tryinclude <ReplayIPStripper_Version>

#if !defined REPLAYIPSTRIPPER_VERSION
#define REPLAYIPSTRIPPER_VERSION "Unknown Version"
#endif

#if !defined REPLAYIPSTRIPPER_URL
#define REPLAYIPSTRIPPER_URL "Built from <Unknown>"
#endif

// =========================================================== //

#define MAX_DIRS 16
#define MAX_IP_LENGTH 16
#define MAX_MAPNAME_LENGTH 64
#define MAX_DISPLAY_LENGTH 12
#define REPLAY_TICKBLOCK_SIZE 7

// =========================================================== //

#include <StringMapEx>
#include <ReplayIPStripper/Replay>

// =========================================================== //