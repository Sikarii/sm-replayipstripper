// =========================================================== //

#include <StringMapEx>

// =========================================================== //

static char VALUE_KEY[] = "value";
static char DISPLAY_KEY[] = "display";

static char display[][] =
{
	"PRO",
	"NUB"
};

// =========================================================== //

methodmap ReplayTeleports < StringMapEx
{
	public ReplayTeleports(int teleportsInt)
	{
		StringMapEx teleports = new StringMapEx();
		teleports.SetValue(VALUE_KEY, teleportsInt);
		
		int index = teleportsInt <= 0 ? 0 : 1;
		teleports.SetString(DISPLAY_KEY, display[index]);
		
		return view_as<ReplayTeleports>(teleports);
	}
	
	property int Value
	{
		public get() { return this.GetInt(VALUE_KEY); }
	}
	
	public void GetDisplay(char[] buffer, int maxlength)
	{
		this.GetString(DISPLAY_KEY, buffer, maxlength);
	}
}

// =========================================================== //