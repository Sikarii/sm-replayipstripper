// =========================================================== //

#include <StringMapEx>

// =========================================================== //

static char VALUE_KEY[] = "value";
static char DISPLAY_KEY[] = "display";

static char display[][] =
{
	"VNL",
	"SKZ",
	"KZT"
};

// =========================================================== //

methodmap ReplayMode < StringMapEx
{
	public ReplayMode(int modeInt)
	{
		StringMapEx mode = new StringMapEx();
		mode.SetValue(VALUE_KEY, modeInt);
		mode.SetString(DISPLAY_KEY, display[modeInt]);
		
		return view_as<ReplayMode>(mode);
	}
	
	property int Value
	{
		public get() { return this.GetInt(VALUE_KEY); }
	}
	
	public void GetDisplay(char[] buffer, int maxlength)
	{
		this.GetString(DISPLAY_KEY, buffer, maxlength);
	}
}

// =========================================================== //