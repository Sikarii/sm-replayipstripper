// =========================================================== //

#include <ReplayIPStripper/ReplayMode>
#include <ReplayIPStripper/ReplayStyle>
#include <ReplayIPStripper/ReplayTickData>
#include <ReplayIPStripper/ReplayTeleports>

// =========================================================== //

#define SUPPORTED_FORMAT_VERSION 0x01

static int MAGIC_NUMBER = 0x676F6B7A;
static char MAGIC_NUMBER_KEY[] = "magic_number";
static char FORMAT_VERSION_KEY[] = "format_version";
static char PLUGIN_VERSION_KEY[] = "plugin_version";

static char MAP_NAME_KEY[] = "map_name";
static char COURSE_KEY[] = "course";
static char MODE_KEY[] = "mode";
static char STYLE_KEY[] = "style";
static char TIME_KEY[] = "time";
static char TELEPORTS_KEY[] = "teleports";
static char STEAM_ID_KEY[] = "steam_id";
static char STEAM_ID2_KEY[] = "steam_id2";
static char IP_KEY[] = "ip";
static char NAME_KEY[] = "name";

static char TICK_COUNT_KEY[] = "tick_count";
static char TICK_DATA_KEY[] = "tick_data";

// =========================================================== //

methodmap Replay < StringMapEx
{
	public Replay(const char[] replayFile)
	{
		if (!FileExists(replayFile))
		{
			LogError("File %s does not exist!", replayFile);
			return null;
		}
		
		int maxlength;
		File file = OpenFile(replayFile, "rb");

		int magicNumber;
		if (!file.ReadInt32(magicNumber) || magicNumber != MAGIC_NUMBER)
		{
			LogError("Failed reading magic number for %s!", replayFile);
			delete file;
			return null;
		}
		
		int formatVersion;
		file.ReadInt8(formatVersion);
		if (formatVersion != SUPPORTED_FORMAT_VERSION)
		{
			LogError("Unsupported format version for %s!", replayFile);
			delete file;
			return null;
		}
		
		// Plugin Version
		file.ReadInt8(maxlength);
		char[] pluginVersion = new char[maxlength + 1];
		file.ReadString(pluginVersion, maxlength, maxlength);
		pluginVersion[maxlength] = '\0';
		
		// Map Name
		file.ReadInt8(maxlength);
		char[] mapName = new char[maxlength + 1];
		file.ReadString(mapName, maxlength, maxlength);
		mapName[maxlength] = '\0';
		
		int course;
		file.ReadInt32(course);
		
		int modeInt;
		file.ReadInt32(modeInt);
		ReplayMode mode = new ReplayMode(modeInt);
		
		int styleInt;
		file.ReadInt32(styleInt);
		ReplayStyle style = new ReplayStyle(styleInt);
		
		float time;
		file.ReadInt32(view_as<int>(time));
		
		int teleportsInt;
		file.ReadInt32(teleportsInt);
		ReplayTeleports teleports = new ReplayTeleports(teleportsInt);
		
		int steamId;
		file.ReadInt32(steamId);
		
		// SteamId2
		file.ReadInt8(maxlength);
		char[] steamId2 = new char[maxlength + 1];
		file.ReadString(steamId2, maxlength, maxlength);
		steamId2[maxlength] = '\0';
		
		// IP
		file.ReadInt8(maxlength);
		char[] ip = new char[maxlength + 1];
		file.ReadString(ip, maxlength, maxlength);
		ip[maxlength] = '\0';
		
		// Name
		file.ReadInt8(maxlength);
		char[] name = new char[maxlength + 1];
		file.ReadString(name, maxlength, maxlength);
		name[maxlength] = '\0';
		
		int tickCount;
		file.ReadInt32(tickCount);

		ReplayTickData tickData = new ReplayTickData();
		for (int i = 0; i < tickCount; i++)
		{
			float position[3];
			file.Read(view_as<int>(position), sizeof(position), 4);
			
			float angles[2];
			file.Read(view_as<int>(angles), sizeof(angles), 4);
			
			int buttons;
			file.ReadInt32(buttons);
			
			int flags;
			file.ReadInt32(flags);

			tickData.PushTick(position, angles, buttons, flags);
		}
		
		StringMapEx replay = new StringMapEx();
		replay.SetValue(MAGIC_NUMBER_KEY, magicNumber);
		replay.SetValue(FORMAT_VERSION_KEY, formatVersion);
		replay.SetString(PLUGIN_VERSION_KEY, pluginVersion);
		replay.SetString(MAP_NAME_KEY, mapName);
		replay.SetValue(COURSE_KEY, course);
		replay.SetValue(MODE_KEY, mode);
		replay.SetValue(STYLE_KEY, style);
		replay.SetValue(TIME_KEY, time);
		replay.SetValue(TELEPORTS_KEY, teleports);
		replay.SetValue(STEAM_ID_KEY, steamId);
		replay.SetString(STEAM_ID2_KEY, steamId2);
		replay.SetString(IP_KEY, ip);
		replay.SetString(NAME_KEY, name);
		replay.SetValue(TICK_COUNT_KEY, tickCount);
		replay.SetValue(TICK_DATA_KEY, tickData);

		delete file;
		return view_as<Replay>(replay);
	}

	property int MagicNumber
	{
		public get() { return this.GetInt(MAGIC_NUMBER_KEY); }
	}

	property int FormatVersion
	{
		public get() { return this.GetInt(FORMAT_VERSION_KEY); }
	}

	public void GetPluginVersion(char[] buffer, int maxlength)
	{
		this.GetString(PLUGIN_VERSION_KEY, buffer, maxlength);
	}

	public void GetMapName(char[] buffer, int maxlength)
	{
		this.GetString(MAP_NAME_KEY, buffer, maxlength);
	}

	property int Course
	{
		public get() { return this.GetInt(COURSE_KEY); }
	}

	property ReplayMode Mode
	{
		public get() { return this.GetAny(MODE_KEY); }
	}

	property ReplayStyle Style
	{
		public get() { return this.GetAny(STYLE_KEY); }
	}

	property float Time
	{
		public get() { return this.GetFloat(TIME_KEY); }
	}

	property ReplayTeleports Teleports
	{
		public get() { return this.GetAny(TELEPORTS_KEY); }
	}

	property int SteamId
	{
		public get() { return this.GetInt(STEAM_ID_KEY); }
	}

	public void GetSteamId2(char[] buffer, int maxlength)
	{
		this.GetString(STEAM_ID2_KEY, buffer, maxlength);
	}
	
	public void SetIP(char[] ip)
	{
		this.SetString(IP_KEY, ip);
	}

	public void GetIP(char[] buffer, int maxlength)
	{
		this.GetString(IP_KEY, buffer, maxlength);
	}

	public void GetName(char[] buffer, int maxlength)
	{
		this.GetString(NAME_KEY, buffer, maxlength);
	}

	property int TickCount
	{
		public get() { return this.GetInt(TICK_COUNT_KEY); }
	}

	property ReplayTickData TickData
	{
		public get() { return this.GetAny(TICK_DATA_KEY); }
	}
	
	public void Clean()
	{
		this.TickData.Clear();
		
		delete this.Style;
		delete this.Mode;
		delete this.TickData;
		delete this.Teleports;
	}

	public bool ExportToFile(char[] file)
	{
		File replay = OpenFile(file, "wb");
		replay.WriteInt32(this.MagicNumber);
		replay.WriteInt8(this.FormatVersion);
		
		char pluginVersion[MAX_DISPLAY_LENGTH];
		this.GetPluginVersion(pluginVersion, sizeof(pluginVersion));
		replay.WriteInt8(strlen(pluginVersion));
		replay.WriteString(pluginVersion, false);
		
		char mapName[MAX_MAPNAME_LENGTH];
		this.GetMapName(mapName, sizeof(mapName));
		replay.WriteInt8(strlen(mapName));
		replay.WriteString(mapName, false);
		
		replay.WriteInt32(this.Course);
		replay.WriteInt32(this.Mode.Value);
		replay.WriteInt32(this.Style.Value);
		replay.WriteInt32(view_as<int>(this.Time));
		replay.WriteInt32(this.Teleports.Value);
		replay.WriteInt32(this.SteamId);
		
		char steamId2[24];
		this.GetSteamId2(steamId2, sizeof(steamId2));
		replay.WriteInt8(strlen(steamId2));
		replay.WriteString(steamId2, false);
		
		char ip[MAX_IP_LENGTH];
		this.GetIP(ip, sizeof(ip));
		replay.WriteInt8(strlen(ip));
		replay.WriteString(ip, false);
		
		char name[MAX_NAME_LENGTH];
		this.GetName(name, sizeof(name));
		replay.WriteInt8(strlen(name));
		replay.WriteString(name, false);
		
		replay.WriteInt32(this.TickCount);
		
		for (int i = 0; i < this.TickCount; i++)
		{
			any tickData[REPLAY_TICKBLOCK_SIZE];
			this.TickData.GetArray(i, tickData, sizeof(tickData));
			
			replay.Write(tickData, sizeof(tickData), 4);
		}
		
		delete replay;
		return true;
	}
}

// =========================================================== //