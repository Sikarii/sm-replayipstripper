// =========================================================== //

#include <StringMapEx>

// =========================================================== //

static char VALUE_KEY[] = "value";
static char DISPLAY_KEY[] = "display";

static char display[][] =
{
	"NRM"
};

// =========================================================== //

methodmap ReplayStyle < StringMapEx
{
	public ReplayStyle(int styleInt)
	{
		StringMapEx style = new StringMapEx();
		style.SetValue(VALUE_KEY, styleInt);
		style.SetString(DISPLAY_KEY, display[styleInt]);
		
		return view_as<ReplayStyle>(style);
	}
	
	property int Value
	{
		public get() { return this.GetInt(VALUE_KEY); }
	}
	
	public void GetDisplay(char[] buffer, int maxlength)
	{
		this.GetString(DISPLAY_KEY, buffer, maxlength);
	}
}

// =========================================================== //