// =========================================================== //

enum
{
	POSITION_X = 0,
	POSITION_Y,
	POSITION_Z,
	ANGLES_X,
	ANGLES_Y,
	BUTTONS,
	FLAGS
};

// =========================================================== //

methodmap ReplayTickData < ArrayList
{
	public ReplayTickData()
	{
		return view_as<ReplayTickData>(new ArrayList(REPLAY_TICKBLOCK_SIZE));
	}
	
	public void PushTick(float position[3], float angles[2], int buttons, int flags)
	{
		any tickData[REPLAY_TICKBLOCK_SIZE];
		tickData[POSITION_X] = position[0];
		tickData[POSITION_Y] = position[1];
		tickData[POSITION_Z] = position[2];
		tickData[ANGLES_X] = angles[0];
		tickData[ANGLES_Y] = angles[1];
		tickData[BUTTONS] = buttons;
		tickData[FLAGS] = flags;
		
		this.PushArray(tickData);
	}
}

// =========================================================== //