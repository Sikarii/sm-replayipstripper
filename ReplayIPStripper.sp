// ====================== DEFINITIONS ======================== //

// ...

// =========================================================== //

#include <ReplayIPStripper>
#include <CommandLineOptions>

#undef REQUIRE_PLUGIN
#include <gokz/localdb>
#include <gokz/replays>

// ====================== FORMATTING ========================= //

#pragma newdecls required

// ====================== VARIABLES ========================== //

ConVar gCV_IPValue = null;
ConVar gCV_AutoStrip = null;
ConVar gCV_ExportPath = null;
ConVar gCV_IgnoreCheaters = null;
CommandlineOptions options = null;

// ======================= INCLUDES ========================== //

// ...

// ====================== PLUGIN INFO ======================== //

public Plugin myinfo =
{
	name = "ReplayIPStripper",
	author = "Sikari",
	description = "",
	version = REPLAYIPSTRIPPER_VERSION,
	url = REPLAYIPSTRIPPER_URL
};

// ======================= MAIN CODE ========================= //

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	gCV_IPValue = CreateConVar("sm_replayipstripper_ipvalue", "0.0.0.0", "IP Address to apply when stripping original one");
	gCV_AutoStrip = CreateConVar("sm_replayipstripper_autostrip", "1", "Automatically strips any new GOKZ replays", _, true, 0.0, true, 1.0);
	gCV_ExportPath = CreateConVar("sm_replayipstripper_exportpath", "{sourcemod}/data/replayipstripper_output", "Path where exported replays go");
	gCV_IgnoreCheaters = CreateConVar("sm_replayipstripper_ignore_cheaters", "1", "Ignore cheaters when auto stripping replays", _, true, 0.0, true, 1.0);
	
	AutoExecConfig(true, "ReplayIPStripper");
	RegAdminCmd("replayipstripper", Command_Main, ADMFLAG_ROOT);
}

public void OnPluginStart()
{
	options = new CommandlineOptions(true);
	options.AddOption("--inputFile", "Input file to parse & strip");
	options.AddOption("--importPath", "Path to import replays from");
	options.AddOption("--exportPath", "Path where exported replays go");
	options.AddOption("--ipaddress", "IP Address to replace when exporting");
}

public Action Command_Main(int client, int args)
{
	char buffer[1024];
	GetCmdArgString(buffer, sizeof(buffer));

	options.Parse(buffer);
	
	if (options.NumDefined <= 0)
	{
		PrintToServer("No arguments provided, see --help for available options");
		return Plugin_Handled;
	}
	
	CommandLineOption cliHelp = options.GetOption("--help");
	
	if (cliHelp.IsDefined)
	{
		char buf[512];
		options.GetHelpBuffer(buf, sizeof(buf));

		PrintToServer(buf);
		return Plugin_Handled;
	}
	
	CommandLineOption cliVersion = options.GetOption("--version");
	
	if (cliVersion.IsDefined)
	{
		PrintToServer("\n------- ReplayIPStripper %s -------", REPLAYIPSTRIPPER_VERSION);
		PrintToServer(" > Supported Replay Format Version \t | %d", SUPPORTED_FORMAT_VERSION);
		PrintToServer(" > Compiled against GOKZ Replay Format \t | %d", RP_FORMAT_VERSION);
		PrintToServer(" > Compiled against CommandLineOptions \t | %s", COMMANDLINEOPTIONS_VERSION);
		PrintToServer(" - %s\n", REPLAYIPSTRIPPER_URL);
		return Plugin_Handled;
	}

	char ipAddress[MAX_IP_LENGTH];
	CommandLineOption cliIPAddress = options.GetOption("--ipaddress");
	
	if (cliIPAddress.IsDefined)
	{
		cliIPAddress.GetVal(ipAddress, sizeof(ipAddress));
	}
	else
	{
		gCV_IPValue.GetString(ipAddress, sizeof(ipAddress));
	}

	char exportPath[PLATFORM_MAX_PATH];
	CommandLineOption cliExportPath = options.GetOption("--exportPath");

	if (cliExportPath.IsDefined)
	{
		cliExportPath.GetVal(exportPath, sizeof(exportPath));
	}
	else
	{
		gCV_ExportPath.GetString(exportPath, sizeof(exportPath));
	}

	CommandLineOption cliInputFile = options.GetOption("--inputFile");
	CommandLineOption cliImportPath = options.GetOption("--importPath");
	
	if (cliInputFile.IsDefined)
	{
		char inputFile[PLATFORM_MAX_PATH];
		cliInputFile.GetVal(inputFile, sizeof(inputFile));
		ExportStrippedReplay(new Replay(inputFile), exportPath, ipAddress);
	}
	else if (cliImportPath.IsDefined)
	{
		char importPath[PLATFORM_MAX_PATH];
		cliImportPath.GetVal(importPath, sizeof(importPath));
		
		int lastChar = strlen(importPath) - 1;
		if (importPath[lastChar] == '/')
		{
			importPath[lastChar] = '\0';
		}

		ArrayList replayFiles = new ArrayList(PLATFORM_MAX_PATH);

		TokenizeSourcemod(importPath);
		IterateDirectory(importPath, replayFiles);
		
		for (int i = 0; i < replayFiles.Length; i++)
		{
			char file[PLATFORM_MAX_PATH];
			replayFiles.GetString(i, file, sizeof(file));
			ExportStrippedReplay(new Replay(file), exportPath, ipAddress);
		}

		delete replayFiles;
	}
	
	return Plugin_Handled;
}

public void GOKZ_RP_OnReplaySaved(int client, const char[] filePath)
{
	if (!gCV_AutoStrip.BoolValue)
	{
		return;
	}
	
	if (GOKZ_DB_IsCheater(client) && gCV_IgnoreCheaters.BoolValue)
	{
		return;
	}
	
	char ipAddress[MAX_IP_LENGTH];
	gCV_IPValue.GetString(ipAddress, sizeof(ipAddress));
	
	char exportPath[PLATFORM_MAX_PATH];
	gCV_ExportPath.GetString(exportPath, sizeof(exportPath));
	
	ExportStrippedReplay(new Replay(filePath), exportPath, ipAddress);
}

static void IterateDirectory(const char[] dir, ArrayList files)
{
	char buffer[PLATFORM_MAX_PATH];
	FileType type = FileType_Unknown;
	
	DirectoryListing dl = OpenDirectory(dir);
	
	while (dl.GetNext(buffer, sizeof(buffer), type))
	{
		if (type == FileType_File)
		{
			if (StrEqual(buffer[strlen(buffer) - 7], ".replay"))
			{
				char path[PLATFORM_MAX_PATH];
				Format(path, sizeof(path), "%s/%s", dir, buffer);

				files.PushString(path);
			}
		}
		else if (type == FileType_Directory)
		{
			if (!StrEqual(buffer, ".") && !StrEqual(buffer, ".."))
			{				
				char newPath[PLATFORM_MAX_PATH];
				Format(newPath, sizeof(newPath), "%s/%s", dir, buffer);
				
				IterateDirectory(newPath, files);
			}
		}
	}

	delete dl;
}

static void ExportStrippedReplay(Replay replay, char exportDir[PLATFORM_MAX_PATH], char ip[MAX_IP_LENGTH] = "0.0.0.0")
{
	if (replay == null)
	{
		return;
	}
	
	replay.SetIP(ip);
	TokenizeSourcemod(exportDir);

	char mapName[MAX_MAPNAME_LENGTH];
	replay.GetMapName(mapName, sizeof(mapName));

	char modeDisplay[MAX_DISPLAY_LENGTH];
	replay.Mode.GetDisplay(modeDisplay, sizeof(modeDisplay));

	char styleDisplay[MAX_DISPLAY_LENGTH];
	replay.Style.GetDisplay(styleDisplay, sizeof(styleDisplay));

	char teleportsDisplay[MAX_DISPLAY_LENGTH];
	replay.Teleports.GetDisplay(teleportsDisplay, sizeof(teleportsDisplay));

	char exportPath[PLATFORM_MAX_PATH];
	Format(exportPath, sizeof(exportPath), "%s/%s/%d_%s_%s_%s.replay",
											exportDir,
											mapName,
											replay.Course,
											modeDisplay,
											styleDisplay,
											teleportsDisplay);
	
	// Ensure our path is good
	CreateDirectories(exportPath);

	replay.ExportToFile(exportPath);
	replay.Clean();
	delete replay;
}

static void TokenizeSourcemod(char path[PLATFORM_MAX_PATH])
{
	char smPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, smPath, sizeof(smPath),"");
	ReplaceString(smPath, sizeof(smPath), "\\", "/");

	// Better way of doing this???
	ReplaceString(path, sizeof(path), "{sourcemod}/", smPath);
	ReplaceString(path, sizeof(path), "{sourcemod}", smPath);
}

static void CreateDirectories(const char[] path)
{
	if (DirExists(path))
	{
		return;
	}
	
	char directories[MAX_DIRS][PLATFORM_MAX_PATH];
	int dirs = ExplodeString(path, "/", directories, sizeof(directories), sizeof(directories[]));
	
	char buffer[MAX_DIRS * PLATFORM_MAX_PATH];
	for (int i = 0; i < dirs - 1; i++)
	{
		Format(buffer, sizeof(buffer), "%s/%s", buffer, directories[i]);

		if (!DirExists(buffer))
		{
			CreateDirectory(buffer, 511);
		}
	}
}

// =========================================================== //
